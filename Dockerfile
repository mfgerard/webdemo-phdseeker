FROM ubuntu:14.04
MAINTAINER Matias Gerard <mgerard@sinc.unl.edu.ar>

# Web Demo Builder - Base Docker image for Python 2.x

ENV python_env="/python_env"
#ENV http_proxy="http://192.168.0.120:3128"

#=============================
# INSTALL BASE PACKAGES
#=============================
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && apt-get install -y --no-install-recommends \
      build-essential \
      pkg-config \
      gfortran \
      libatlas-base-dev \
      libatlas3gf-base \
      fonts-lyx \
      libfreetype6-dev \
      libpng-dev \
      python2.7 \
      python-dev \
      python-pip \
      python-tk \
      tk-dev \
      libyaml-dev \
      imagemagick && \
    rm -rf /var/lib/apt/lists/*

#=============================
# INSTALL PYTHON PACKAGES
#=============================
RUN pip install -U virtualenv==12.0.7
RUN pip install -U pip==9.0.1
RUN pip install -U setuptools==36.2.2
RUN pip install -U numpy==1.13.0
RUN pip install -U scipy==0.19.1
RUN pip install -U pyrapidjson==0.4.1
RUN pip install -U pyyaml==3.11
RUN pip install -U multiprocessing==2.6.2.1
RUN pip install -U matplotlib==2.0.2


RUN virtualenv ${python_env}

COPY install_python_module /usr/local/bin/
# RUN install_python_module pip==9.0.1
# RUN install_python_module setuptools==36.2.2
# RUN install_python_module numpy==1.13.0
# RUN install_python_module scipy==0.19.1
# RUN install_python_module pyrapidjson==0.4.1
# RUN install_python_module pyyaml==3.11
# RUN install_python_module multiprocessing==2.6.2.1
# RUN install_python_module matplotlib==2.0.2

RUN ln -s ${python_env}/bin/python /usr/local/bin/python

# Create a new user "developer".
# It will get access to the X11 session in the host computer

ENV uid=1000
ENV gid=${uid}

COPY init.sh /
COPY create_user.sh /
COPY matplotlibrc_tkagg /
COPY matplotlibrc_agg /

ENTRYPOINT ["/init.sh"]
CMD ["/create_user.sh"]
